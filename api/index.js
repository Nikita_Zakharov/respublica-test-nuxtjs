import urls from "@/api/urls";

const createApi = ({$axios}) => {
    return {
        products: {
            getById: async (id) => await $axios.$get(`${urls.getItems}${id}`)
        },
        knigi: {
            getAll: async (query = '') => await $axios.$get(`${urls.knigi}?${query}`)
        }
    }
}

export {createApi}
