const errorHandler = (error) => {
  console.error(error)
  return Promise.reject(error);
}

export default function ({ app, $axios }) {
  $axios.interceptors.response.use(response => response, errorHandler);
}
