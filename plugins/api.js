import {createApi} from "@/api";

export default function({app, $axios}, inject) {
    // $axios.interceptors.response.use(response => response, errorHandler);

    inject('api', createApi({$axios}))
}
