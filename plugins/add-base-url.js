export default ({app}, inject) => {
    inject('addBaseURL', url => {
        if (process.env.baseURL) {
            return `${process.env.baseURL}${url}`
        }
        console.error('process.env.baseURL not found')
        return url
    })
}
