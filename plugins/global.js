import Vue from 'vue';
import CButton from "@/components/CButton/CButton";

export default function () {
    Vue.component('CButton', CButton)
}
