// eslint-disable-next-line nuxt/no-cjs-in-config
module.exports = {
    env: {
        baseURL: process.env.baseURL || 'https://www.respublica.ru',
        apiURL: `${process.env.baseURL}/api/v1/` || 'https://www.respublica.ru/api/v1/'
    },
    /*
    ** Headers of the page
    */
    head: {
        htmlAttrs: {
            lang: 'ru'
        },
        title: 'Книжный магазин',
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico'
            }
        ]
    },
    /*
    ** Customize the progress bar color
    */
    loading: {color: '#46c3d2'},
    /*
    ** Modules
     */
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/style-resources'
    ],
     axios: {
        baseURL: process.env.baseUrl ? `${process.env.baseUrl}/api/v1/` : 'https://www.respublica.ru/api/v1/'
    },
    styleResources: {
        scss: [
            '@/assets/scss/utils/variables.scss',
            '@/assets/scss/utils/mixins.scss'
        ]
    },
    /*
    ** Global CSS
    */
    css: [
        'swiper/swiper.scss',
        '@/assets/scss/utils/normalize.scss',
        '@/assets/scss/utils/space-helpers.scss',
        '@/assets/scss/utils/fonts.scss',
        '@/assets/scss/utils/helpers.scss',
        '@/assets/scss/common.scss',
        '@/assets/scss/plugins.scss'
    ],
    plugins: [
        '@/plugins/event-bus',
        '@/plugins/global',
        '@/plugins/swiper',
        '@/plugins/add-base-url',
        '@/plugins/axios',
        '@/plugins/api'
    ],
    buildModules: [
        ['@nuxtjs/stylelint-module', { /* module options */}],
        ['@nuxtjs/router', {
            path: 'router',
            fileName: 'index.js'
        }]
    ],
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        extend(config, {isDev, isClient}) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    }
}

