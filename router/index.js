import Vue from 'vue';
import VueRouter from 'vue-router'

import Home from '@/pages/index.vue'
import Cart from '@/pages/cart.vue'
import Product from '@/pages/product.vue'

Vue.use(VueRouter);

export function createRouter() {
    return new VueRouter({
        mode: 'history',
        routes: [
            {
                path: '/',
                component: Home,
                name: 'home'
            },
            {
                path: '/cart',
                component: Cart,
                name: 'cart'
            },
            {
                path: '/product/:id',
                component: Product,
                name: 'product'
            },
        ],
        scrollBehavior (to, from, savedPosition) {
            return { x: 0, y: 0 }
        }
    })
}
