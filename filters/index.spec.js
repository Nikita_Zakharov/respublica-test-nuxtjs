import {formatWithSeparator} from './index'

describe('numbers that are greater than 1000, should be with spaces', () => {
    expect(formatWithSeparator(55350)).toBe('55 350')
    expect(formatWithSeparator(355350)).toBe('355 350')
    expect(formatWithSeparator(1550000)).toBe('1 550 000')
    expect(formatWithSeparator(12550000)).toBe('12 550 000')
    expect(formatWithSeparator(125500000)).toBe('125 500 000')
})

describe('numbers that are smaller than 1000, should does not change', () => {
    expect(formatWithSeparator(3)).toBe('3')
    expect(formatWithSeparator(30)).toBe('30')
    expect(formatWithSeparator(300)).toBe('300')
})

test('formatWithSeparator should return undefined', () => {
    expect(formatWithSeparator()).toBeUndefined()
})

