export const firstLetterCapitalize = (str = '') => {
    if (str.length > 0) {
        return `${str[0].toUpperCase()}${str.slice(1)}`
    }
    return str
}

export const formatWithSeparator = (price, separator = " ") => {
    if (price) {
      return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
    }
}

