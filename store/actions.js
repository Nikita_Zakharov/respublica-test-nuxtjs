export default {
    addProductToCart({state, commit}, product) {
        const currentCart = [...state.cart];
        currentCart.push(product);
        commit('SET_CART', currentCart)
    }
}
