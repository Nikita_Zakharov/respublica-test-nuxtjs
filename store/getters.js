export default {
    isProductInCart: state => productId => {
        return state.cart.some(product => product.id === productId);
    },
    totalPrice(state) {
        return state.cart
            .map(product => product.price)
            .reduce((acc, item) => acc + item, 0)
    }
}
